<?php

namespace App\Controller;

use App\Repository\PinRepository;
use App\Entity\Pin;
use App\Form\PinType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class PinsController extends AbstractController
{
    /**
     * @Route("/", name="app_home", methods="GET")
     */
    public function index(PinRepository $repo, PaginatorInterface $paginator, Request $request): Response
    {
        $pins = $repo->findBy([],['createdAt' => "DESC"]);
        $pins = $paginator->paginate(
            // Doctrine Query, not results
            $pins,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            12
        );
        return $this->render(
            "pins/index.html.twig",
            compact('pins')
        );
    }

    /**
     * @Route("/pins/{id_pin<[0-9]+>}", name="app_pins_show", methods="GET")
     */
    public function show(PinRepository $repo, int $id_pin): Response
    {
        $pin = $repo->find($id_pin);

        return $this->render(
            "pins/show.html.twig",
            compact('pin')
        );
    }

    
    /**
     * @Route("/pins/{pin<[0-9]+>}/edit", name="app_pins_edit", methods="GET|PUT")
     * @Security("is_granted('PIN_EDIT', pin)")
     */
    public function edit(Pin $pin, Request $request, EntityManagerInterface $em): Response
    {
        if ($pin->getUser() != $this->getUser()){
            $this->addFlash("error","Access forbidden");
            return $this->redirectToRoute("app_home");
        }

        $form = $this->createForm(PinType::class, $pin, [
            'method' => 'PUT'
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $em->flush();

            $this->addFlash("success","Pin edited!");

            return $this->redirectToRoute("app_home");
        }

        return $this->render(
            "pins/edit.html.twig",
            [
                'form' => $form->createView(),
                'pin' => $pin
            ]
        );
    }

    
    /**
     * @Route("/pins/create", name="app_pins_create", methods={ "GET", "POST" })
     * @Security("is_granted('ROLE_USER') && user.isVerified()")
     */
    public function create(Request $request, EntityManagerInterface $em, UserRepository $userRepo): Response
    {
        $pin = new Pin;
        $form = $this->createForm(PinType::class, $pin);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $pin->setUser($this->getUser());
            $em->persist($pin);
            $em->flush();

            $this->addFlash("success","Pin created!");

            return $this->redirectToRoute("app_home");
        }

        return $this->render(
            "pins/create.html.twig",
            [
                "form" => $form->createView(),
            ]
        );
    }


    /**
     * @Route("/pins/{pin<[0-9]+>}/delete", name="app_pins_delete", methods="GET|DELETE")
     * @IsGranted("PIN_DELETE", subject="pin")
     */
    public function delete(Pin $pin, Request $request, EntityManagerInterface $em): Response
    {        
        // $this->denyAccessUnlessGranted("PIN_DELETE", $pin);

        if ($this->isCsrfTokenValid('pin_deletion'.$pin->getId(), $request->request->get('csrf_token'))){
             $em->remove($pin);
            $em->flush();
        }

        $this->addFlash("info","Pin deleted!");

        return $this->redirectToRoute("app_home");
    }
}
