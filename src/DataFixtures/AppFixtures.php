<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Pin;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private UserPasswordEncoderInterface $passwordEncoder;

    function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User;
        $user->setFirstName("John");
        $user->setLastName("Doe");
        $user->setEmail("johndoe@example.com");
        $user->setPassword($this->passwordEncoder->encodePassword($user, "john"));
        $user->setIsVerified(true);
        $manager->persist($user);

        $user2 = new User;
        $user2->setFirstName("Jane");
        $user2->setLastName("Doe");
        $user2->setEmail("janedoe@example.com");
        $user2->setPassword($this->passwordEncoder->encodePassword($user2, "jane"));
        $user2->setIsVerified(true);
        $manager->persist($user2);

        $admin = new User;
        $admin->setFirstName("Admin");
        $admin->setLastName("Admin");
        $admin->setEmail("admin@admin.com");
        $admin->setPassword($this->passwordEncoder->encodePassword($admin, "admin"));
        $admin->setIsVerified(true);
        $admin->addRole("ROLE_ADMIN");
        $manager->persist($admin);

        for ($i = 1; $i <= 20; $i++) {
            $pin = new Pin;
            $pin->setTitle('Pin '.$i);
            $pin->setDescription("Le super pin n°".$i);
            if ($i%2 == 0){
                $pin->setUser($user);
            } else {
                $pin->setUser($user2);
            }
            
            $manager->persist($pin);
        }

        $manager->flush();
    }
}
