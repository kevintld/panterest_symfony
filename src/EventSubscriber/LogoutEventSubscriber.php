<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Http\Event\LogoutEvent;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class LogoutEventSubscriber implements EventSubscriberInterface
{

    private UrlGeneratorInterface $url;
    private FlashBagInterface $flashbag;

    public function __construct(UrlGeneratorInterface $url, FlashBagInterface $flashbag){
        $this->url = $url;
        $this->flashbag = $flashbag;
    }

    public function onLogoutEvent(LogoutEvent $event)
    {
        //$event->getRequest()->getSession()->getFlashBag()->add("success","You have been disconnected successfully!");
        $this->flashbag->add("success","Bye Bye " . $event->getToken()->getUser()->fullName());
        $event->setResponse(new RedirectResponse($this->url->generate('app_home')));
    }

    public static function getSubscribedEvents()
    {
        return [
            LogoutEvent::class => 'onLogoutEvent',
        ];
    }
}
